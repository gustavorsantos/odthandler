# Formas de substituição
Temos 2 formas de substituir um elemento

## 1. Método replaceChild()
Aqui o xml já troca um elemento por outro
```
    new_tag = doc.createElement("text:span")
    new_tag.setAttribute('text:style-name', a[0].getAttribute('text:style-name'))
    new_tag.appendChild(doc.createTextNode('SUBSTITUIDO'))
    parent = item.parentNode
    parent.replaceChild(new_tag, item)
```

## 2. Inserindo antes e excluindo
Já aqui o XML adiciona o novo elemento antes daquele que se deseja excluir e depois o exclui
```
    new_tag = doc.createElement("text:span")
    new_tag.setAttribute('text:style-name', a[0].getAttribute('text:style-name'))
    new_tag.appendChild(doc.createTextNode('SUBSTITUIDO'))
    parent = item.parentNode
    parent.insertBefore(new_tag, item)
    parent.removeChild(item)
```