import zipfile
from io import BytesIO

def open_odt(path):
    unzipped = zipfile.ZipFile(path, 'a')
    return unzipped

def write_odt(unzipped, content, path):
    archive = BytesIO()
    file_list = unzipped.namelist()

    with zipfile.ZipFile(archive, 'w') as zip_archive:
        # Create three files on zip archive
        
        for filename in file_list:
            if(filename != 'content.xml'):
                zip_archive.writestr(filename, unzipped.open(filename).read())

        zip_archive.writestr('content.xml', content)

    with open(path+'/result.odt', 'wb') as f:
        f.write(archive.getbuffer())
    unzipped.close( )

def replace_text(xml, old, new, tag):
    elms = xml.getElementsByTagName(tag)
    for item in elms:
        if(item.childNodes[0].nodeValue == old):
            new_tag = xml.createElement(tag)
            new_tag.setAttribute('text:style-name', item.getAttribute('text:style-name'))
            new_tag.appendChild(xml.createTextNode(new))
            parent = item.parentNode
            parent.replaceChild(new_tag, item)

def fill_table(xml, array, numTable):
    selectTable = None
    tables = xml.getElementsByTagName("table:table")
    for table in tables:
        if(table.getAttribute('table:name') == 'Table'+str(numTable)):
            selectTable = table
            break
    rows = selectTable.getElementsByTagName("table:table-row")
    j = 0
    for row in range(1, len(rows)):
        cells = rows[row].getElementsByTagName("table:table-cell")
        for cell in cells:
            cell_texts = cell.getElementsByTagName("text:p")
            for text in cell_texts:
                if(len(text.childNodes) != 0):
                    for child in text.childNodes:               # If the table cell is not empty, it cleans
                        text.removeChild(child)
                text.appendChild(xml.createTextNode(array[j]))
                j = j + 1