from odt import open_odt, write_odt, replace_text, fill_table
from xml.dom.minidom import parseString

array = [
'04/03/21','23','27',
'05/03/21','24','28',
'06/03/21','22','26',
'07/03/21','25','30',
'08/03/21','30','33',
'09/03/21','27','31',
'10/03/21','20','25',
'11/03/21','19','24',
'12/03/21','24','28',
'13/03/21','31','34',
'14/03/21','29','33',
'15/03/21','23','28',
'16/03/21','22','26',
'17/03/21','21','25',
]

unzipped = open_odt('/home/gustavo/Documents/odt/template.odt')
content = unzipped.read('content.xml')
doc = parseString( content.decode('utf-8') )


replace_text(doc, '[data_inicio]', array[0], 'text:span')
replace_text(doc, '[data_fim]', array[39], 'text:span')
fill_table(doc, array, 1)

write_odt(unzipped, doc.toxml(), '/home/gustavo/Documents/odt')